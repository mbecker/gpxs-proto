cls
SET SRC_DIR=C:\apps\go\src\gitlab.com\mbecker\gpxs-proto
SET DST_DIR0=C:\apps\go\src\gitlab.com\mbecker\gpxs-proto\proto
SET DST_DIR1=C:\apps\go\src\gitlab.com\mbecker\gpxs-maps
SET DST_DIR2=C:\apps\go\src\gitlab.com\mbecker\gpxs-http-upload
SET DST_DIR3=C:\apps\go\src\gitlab.com\mbecker\gpxs-api
SET PROTO_PATH=C:\apps\go\src\gitlab.com\mbecker\gpxs-proto\gpxmessage_v9.proto
protoc -I=%SRC_DIR% --go_out=%DST_DIR0% %PROTO_PATH%
:: --go_out=%DST_DIR2% --go_out=%DST_DIR3% 